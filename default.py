from resources.lib.espnplayer import *

import sys
import urllib
import xbmcgui
import xbmc
import xbmcaddon
import xbmcplugin

def add_dir(name, url, mode, iconimage, isfolder):
    u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+ mode

    liz=xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": name } )
    xbmcplugin.addDirectoryItem(int(sys.argv[1]), u, liz, isfolder)

def get_params():
    param=[]
    paramstring=sys.argv[2]
    print sys.argv[2]
    if len(paramstring)>=2:
        params=sys.argv[2]
        cleanedparams=params.replace('?','')
        if (params[len(params)-1]=='/'):
            params=params[0:len(params)-2]
        pairsofparams=cleanedparams.split('&')
        param={}
        for i in range(len(pairsofparams)):
            splitparams={}
            splitparams=pairsofparams[i].split('=')
            if (len(splitparams))==2:
                param[splitparams[0]]=splitparams[1]

    return param

addon = xbmcaddon.Addon()
path = xbmc.translatePath(addon.getAddonInfo('path'))
addon_data_path = xbmc.translatePath(addon.getAddonInfo('profile'))
icon = path + "/icon.png"

def add_link(name, url, title, iconimage):
    if iconimage == '':
        iconimage = icon
    liz=xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": title } )
    xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=url,listitem=liz)

def show_games(espn_client, category, product='ESPN_COLLEGE_PASS'):
    games_data = espn_client.get_games(category, product)
    games = games_data.games

    for game in games:
        if game.game_status != 'upcoming':
            if game.sport_code != 'NCAA Shows':
                add_dir(str(game), sys.argv[0] + game.airring_id, category, game.game_image, True)
        else:
            add_link(str(game), "", "", game.game_image)

def show_stream_links(streams):
    for entry in streams:
        label, stream, value = entry
        add_link(label, stream, label, "")


params = get_params()
print params

try:
    url=urllib.unquote_plus(params["url"])
except:
    print "exception url"
    url=None

try:
    mode = params["mode"]
except:
    print "exception mode"
    mode = None

print "Mode: "+str(mode)
print "URL: "+str(url)
print addon_data_path

if addon.getSetting('username') and addon.getSetting('password'):
    espn_client = EspnPlayer(addon.getSetting('username'), addon.getSetting('password'), addon_data_path)
    if espn_client.check_login():
        print 'espn_client.check_login() : returned true'
        if mode == None or url==None or len(url)<1:
            add_dir("SEC Network LIVE", sys.argv[0], "sec", icon, True)
            add_dir("ESNPU LIVE", sys.argv[0], "espnu", icon, True)
            show_games(espn_client, 'today')
            add_dir("Archive", sys.argv[0], "show_archive", icon, True)
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
        elif mode == 'today':
            game_id = url.rsplit('/',1)[1]
            show_stream_links(espn_client.get_game_streams(game_id))
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
        elif mode == 'sec':
            show_stream_links(espn_client.get_sec_streams())
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
        elif mode == 'espnu':
            show_stream_links(espn_client.get_espnu_streams())
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
        elif mode == 'show_archive':
            show_games(espn_client, 'archive')
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
        elif mode == 'archive':
            game_id = url.rsplit('/',1)[1]
            streams = espn_client.get_game_streams(game_id)
            show_stream_links(streams)
            xbmcplugin.endOfDirectory(int(sys.argv[1]))
    else:
        print 'espn_client.check_login() : returned false'
        dialog = xbmcgui.Dialog()
        dialog.ok('Login failed', 'Check your login credentials')
        addon.openSettings()
        xbmcplugin.endOfDirectory(handle = int(sys.argv[1]),succeeded=False)
else:
    addon.openSettings()
    xbmcplugin.endOfDirectory(handle = int(sys.argv[1]),succeeded=False)
