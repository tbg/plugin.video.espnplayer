import cookielib
import urllib2
import os
import urllib
import simplejson
import xmltodict
import time
import m3u8

class EspnPlayer:

    def __init__(self, user, password, working_dir='.'):
        self.user = user
        self.password = password
        self.working_dir = working_dir

    def login(self):
        cj = cookielib.LWPCookieJar(os.path.join(self.working_dir, 'auth.lwp'))
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        opener.addheaders = [('Content-type', 'application/x-www-form-urlencoded'), 
                             ('User-Agent','ESPN Player 5.0820 (iPad; iPhone OS 8.4.1; en_RO)')]
        login_data = urllib.urlencode({'username': self.user, 'password': self.password})
        r=opener.open('https://www.espnplayer.com/espnplayer', login_data)
        print "login returned: %s" % r.getcode()

        if r.getcode() == 200:
            cj.save(ignore_discard=True, ignore_expires=True)
            return True
        else:
            return False

    def check_login(self):
        if self.is_login_cookie_valid():
            print 'login cookie is valid'
            return True
        else:
            return self.login()

    def get_games(self, category, product):
        url = 'http://www.espnplayer.com/espnplayer/servlets/games?category=%s&product=%s&format=json' % (category, product)

        opener = urllib2.build_opener()
        opener.addheaders.append(('Cookie', 'JSESSIONID=%s' % self.get_login_cookie()))
        response = opener.open(url)

        games_json = simplejson.loads(response.read())
        game_data = EspnPlayerGameData(games_json)
        return game_data
  
    def get_pkan(self, game_id):
        url = 'http://neulion.go.com/espngeo/dgetpkan?airingId=%s' % game_id
        response = urllib2.urlopen(url)
        pkan = response.read()
        return pkan

    def start_espnu_session(self, pkan):
        login_data = urllib.urlencode({'playerId': 'neulion',
                                       'playbackScenario': 'HTTP_CLOUD_TABLET',
                                       'tokenType': 'GATEKEEPER',
                                       'channel':'espnu',
                                       'pkanType':'TOKEN',
                                       'simulcastAiringId' : '0',
                                       'ttl':'480',
                                       'pkan' : pkan})
        return self.open_session(login_data)

    def start_sec_session(self, pkan):
        login_data = urllib.urlencode({'playerId': 'neulion',
                                       'playbackScenario': 'HTTP_CLOUD_TABLET',
                                       'tokenType': 'GATEKEEPER',
                                       'channel':'sec',
                                       'pkanType':'TOKEN',
                                       'simulcastAiringId' : '0',
                                       'ttl':'480',
                                       'pkan' : pkan})
        return self.open_session(login_data)

    def open_session(self, login_data):
        url = 'http://neulion.go.com/espngeo/startSession'
        cj = cookielib.LWPCookieJar(os.path.join(self.working_dir, 'session.lwp'))
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        opener.addheaders = [('Content-type', 'application/x-www-form-urlencoded'),
                             ('User-Agent', 'ESPN%20Player/5.0820 CFNetwork/711.5.6 Darwin/14.0.0')]
        r=opener.open(url, login_data)

        #Save the cookie
        cj.save(ignore_discard=True)
        return r.read()

    def start_session(self, game_id, pkan):
        login_data = urllib.urlencode({'channel': 'espn3',
                                       'playbackScenario': 'HTTP_CLOUD_WIRED',
                                       'simulcastAiringId' : game_id,
                                       'playerId' :  'neulion',
                                       'pkanType' : 'TOKEN',
                                       'tokenType' : 'GATEKEEPER',
                                       'ttl' : '480',
                                       'pkan' : pkan})
        return self.open_session(login_data)
 
    def is_login_cookie_valid(self):
        login_cookie_file = os.path.join(self.working_dir, 'auth.lwp')
        if os.path.isfile(login_cookie_file):
            cj = cookielib.LWPCookieJar()
            cj.load(login_cookie_file, ignore_discard=True)
            expired = False
            for cookie in cj:
                expired = expired or cookie.is_expired()
            return not expired
        else:
            print login_cookie_file + " does not exists"
            return False

    def streams_from_m3u8(self, m3u8_url, encoded_header):
        print "m3u8_url: " + m3u8_url
        m3u8_obj = m3u8.load(m3u8_url)
        streams = []
        if m3u8_obj.is_variant:
            for playlist in m3u8_obj.playlists:
                stream_info = playlist.stream_info
                if stream_info.resolution:
                    resolution = "%sx%s" % (stream_info.resolution[0], stream_info.resolution[1])
                    streams.append((resolution, playlist.uri+ '|' + encoded_header, stream_info.resolution[1]))
                else:
                    streams.append(("%s" % self.sizeof_fmt(stream_info.bandwidth), playlist.uri + '|' + encoded_header, stream_info.bandwidth))

        return sorted(streams, key=lambda stream: stream[2], reverse=True)

    def ensure_login(self):
        if self.is_login_cookie_valid() == False:
            self.login()

    def get_m3u8_url(self, start_session_response):
        doc = xmltodict.parse(start_session_response)
        return doc['user-verified-media-response']['user-verified-event']['user-verified-content']['user-verified-media-item']['url']

    def get_espnu_streams(self):
        self.ensure_login()
        response = self.start_espnu_session(self.get_pkan('espnu'))
        m3u8_url = self.get_m3u8_url(response)
        encoded_header = urllib.urlencode(self.get_session_header())
        return self.streams_from_m3u8(m3u8_url, encoded_header)

    def sizeof_fmt(self, num, suffix='B'):
        for unit in ['','K']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'M', suffix)

    def get_sec_streams(self):
        self.ensure_login()
        response = self.start_sec_session(self.get_pkan('sec'))
        m3u8_url = self.get_m3u8_url(response)
        encoded_header = urllib.urlencode(self.get_session_header())
        return self.streams_from_m3u8(m3u8_url, encoded_header)

    def get_session_header(self):
        cookies = self.get_session_cookies()
        login_cookie = self.get_login_cookie()

        header = {'Cookie' : cookies,
                  'User-Agent' : 'AppleCoreMedia/1.0.0.12H321 (iPad; U; CPU OS 8_4_1 like Mac OS X; en_us)',
                  'Accept-Encoding' : 'identity',
                  'Connection' : 'keep-alive',
                  'X-Playback-Session-Id': login_cookie
                 }
        return header

    def get_game_streams(self, airring_id):
        self.ensure_login()
        response = self.start_session(airring_id, self.get_pkan(airring_id))
        m3u8_url = self.get_m3u8_url(response)
        encoded_header = urllib.urlencode(self.get_session_header())
        return self.streams_from_m3u8(m3u8_url, encoded_header)

    def get_session_cookies(self):
        cj = cookielib.LWPCookieJar()
        cj.load(os.path.join(self.working_dir, 'session.lwp'), ignore_discard=True)
        cookies = ''
        for cookie in cj:
            cookies = cookies + cookie.name + "=" + cookie.value + "; "

        return cookies

    def get_login_cookie(self):
        cj = cookielib.LWPCookieJar()
        cj.load(os.path.join(self.working_dir, 'auth.lwp'), ignore_discard=True)
        cookie_value = ''
        for cookie in cj:
            if cookie.name == 'JSESSIONID':
                cookie_value = cookie.value

        return cookie
  
  
class EspnPlayerGameData:
    def __init__(self, game_data_json):
        self.total_pages = game_data_json['totalPage']
        self.current_page = game_data_json['currentPage']
        self.games = self.parse_games(game_data_json['games'])

    def parse_games(self, games_json):
        games = []

        for game_json in games_json:
            games.append(Game(game_json))

        return games


class Game:

    def __init__(self, game_json):
        self.name = game_json['name']
        self.game_id = game_json['game_id']
        self.airring_id = game_json['airring_id']
        self.game_status = game_json['game_status']
        self.home_display = game_json['home_display']
        self.away_display = game_json['away_display']
        self.game_image = game_json['game_image']
        self.game_date_local = game_json['game_date']
        self.sport_code = game_json['sportCode']

    def get_status(self):
        if self.game_status == 'inplay':
            return 'LIVE'
        elif self.game_status == 'liveend':
            return 'FINAL'
        else:
            return self.get_formatted_time(self.game_date_local)

    def get_formatted_time(self, timestamp):
        time_local = time.strptime(timestamp, "%Y-%m-%dT%H:%M:%S")
        return time.strftime("%b %d %I:%M %p", time_local)    

    def __str__(self):
        if self.name:
            return self.get_status() + ": " + self.name
        else:
            if self.sport_code == 'NCAA Shows':
                return self.get_status() + ": " + self.away_display + " " + self.home_display
            else:
                return self.get_status() + ": " + self.away_display + " vs. " + self.home_display + " (" + self.sport_code + ")"
